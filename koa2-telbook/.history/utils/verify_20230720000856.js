/**
 * token 验证
 */
const jwt = require('jsonwebtoken')
module.exports = (...args) => {
    return new Promise((resolve, reject) => {
        jwt.verify(...args, (error, decoded) => {
            console.log(args)
            error ? reject(error) : resolve(decoded)
        })
    })
}

const { exec } = require('../db/mysql')
const { genPassword } = require('../utils/cryp')

const register = async (username, password) => {
    // 生成加密密码
    console.log(username, password, '注册')
    username = username
    password = genPassword(password)
    const sql = `INSERT INTO users (username, password) VALUES ('${username}', '${password}')`
    const insertData = await exec(sql)
    console.log(`insertData: ${insertData}`)
    return {
        id: insertData.insertId
    }
}
// 校验用户
const userNameFilter = async (username) => {
    const sql = `select username from users where username='${username}'`
    const row = await exec(sql)
    return row[0] || ''
}

// 校验用户信息
const userInfo = async (id) => {
    const sql = `select id, username from users where id=${id}`
    const row = await exec(sql)
    console.log(row)
    return row[0] || ''
}
// 登录
const login = async (username, password) => {
    // 生成加密密码
    password = genPassword(password)

    const sql = `select id, username, level from users where username='${username}' and password`
    const rows = await exec(sql)
    return rows[0] || ''

}



module.exports = {
    login,
    register,
    userNameFilter,
    userInfo
}
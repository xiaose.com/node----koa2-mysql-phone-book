const { exec } = require('../db/mysql')

// 查询列表数据
const getList = async (name, createUserId) => {
    let sql = `select * from phones where create_user_id="${createUserId}" and `
    if (name) {
        sql += `name like '%${name}%' or tel like '%${name}%' or remark like '%${name}%'`
    } else {
        sql += '1=1 '
    }
    // if (name) {
    //     sql += `or remark like '%${name}%' `
    // }

    sql += `order by createtime desc;`
    console.log(sql)
    return await exec(sql)
}

const getPhone = async (tel, createId) => {
    const sql = `select * from phones where tel=${tel} and create_user_id=${createId}`
    return await exec(sql)


}
const getPhoneItem = async (id, createId) => {
    const sql = `select * from phones where id=${id} and create_user_id=${createId}`
    return await exec(sql)


}

// 新增
const addPhone = async (name, tel, remark = "") => {
    // if (tel) {
    //     const sql = `select * from phones where tel=${tel}`
    //     let aac = await exec(sql)
    //     console.log(aac)
    //     return;
    // }

    const sql = `INSERT INTO phones set(name, tel, remark) VALUES ('${name}', '${tel}', '${remark}')`

    // // let sql = `select * from phones where 1=1 `
    // if (name) {
    //     sql += `and name='${name}' `
    // }
    // if (content) {
    //     sql += `and content like '%${content}%' `
    // }

    // sql += `order by createtime desc;`
    console.log(sql)
    return await exec(sql)
}
const editPhone = async (id, name, tel, remark = "") => {
    const sql = `update phones set name='${name}', tel='${tel}', remark='${remark}' where id=${id}`

    console.log(sql)
    return await exec(sql)
}
module.exports = {
    getList,
    addPhone,
    editPhone,
    getPhone,
    getPhoneItem
}
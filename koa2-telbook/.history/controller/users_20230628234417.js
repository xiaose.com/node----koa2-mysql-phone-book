
const { exec } = require('../db/mysql')
const { genPassword } = require('../utils/cryp')

const register = async (username, password) => {
    // 生成加密密码
    username = username
    password = genPassword(password)
    const sql = `
        INSERT INTO users (username, password) VALUES ('${username}', '${password}')
    `
    const insertData = await exec(sql)
    console.log(`insertData: ${insertData}`)
    return {
        id: insertData.insertId
    }
}
const userNameFilter = async (username) => {
    const sql = `
        select id, username from users where username='${username}'
    `
    const row = await exec(sql)
    return row[0] || ''
}

module.exports = {
    register,
    userNameFilter,
}
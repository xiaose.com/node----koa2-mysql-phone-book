const { exec } = require('../db/mysql')

const getList = async (name, content) => {
    let sql = `select * from phones`
    if (name) {
        sql += `and name=${name} `
    }
    if (content) {
        sql += `and content like '%${content}%' `
    }

    // sql += `order by createtime desc`
    console.log(sql)
    return await exec(sql)
}

module.exports = {
    getList
}
const env = process.env.NODE_ENV

console.log(env)
let MYSQL_CONF

let AUTORIZATION = {
    jwtSecret: 'jwtSercret', //  token 密钥
    tokenExpresTime: 360 * 7   //  7天有效期
}

if (env === 'env') {
    MYSQL_CONF = {
        host: 'localhost',
        user: 'root',
        password: '',
        port: '3306',
        database: 'book'
    }
}

if (env === 'production') {
    MYSQL_CONF = {
        host: 'localhost',
        user: 'root',
        password: '',
        port: '3306',
        database: 'book'
    }
}

module.exports = {
    MYSQL_CONF
}

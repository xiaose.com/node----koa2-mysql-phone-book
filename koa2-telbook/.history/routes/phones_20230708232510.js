const router = require('koa-router')()

const {
    getList,
    addPhone
} = require('../controller/phones')

const { SuccessModel, ErrorModel } = require('../model/resModel')

router.prefix('/api/phones')

router.get('/list', async function (ctx, next) {
    let name = ctx.query.name || ''
    const remark = ctx.query.remark || ''
    const listData = await getList(name, remark)
    console.log(listData)
    ctx.body = new SuccessModel(listData)

})

router.post('/add', async function (ctx, next) {
    console.log(ctx)
    let name = ctx.request.body.name || ''
    let tel = ctx.request.body.tel || ''
    if (!name && !tel) {
        ctx.body = new ErrorModel('名字和号码不能和为空')
        return
    }
    let remark = ctx.request.body.remark || ''
    const listData = await addPhone(name, tel, remark)
    console.log(listData)
    ctx.body = new SuccessModel(listData)

})

module.exports = router
const router = require('koa-router')()

const {
    getList
} = require('../controller/phones')

router.prefix('api/phones')

router.get('/list', async function (ctx, next) {
    let name = ctx.query.name || ''
    const content = ctx.query.content || ''
    const listData = await getList(name, content)
    ctx.body = listData

})

module.exports = router
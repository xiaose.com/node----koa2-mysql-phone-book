const router = require('koa-router')()

const {
    getList
} = require('../controller/phones')

const { SuccessModel, ErrorModel } = require('../model/resModel')

router.prefix('/api/phones')

router.get('/list', async function (ctx, next) {
    let name = ctx.query.name || ''
    const content = ctx.query.content || ''
    const listData = await getList(name, content)
    console.log(listData)
    ctx.body = new SuccessModel(listData)

})

module.exports = router
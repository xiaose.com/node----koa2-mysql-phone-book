const router = require('koa-router')()

const { SuccessModel, ErrorModel } = require('../model/resModel')

router.prefix('/api/users')

router.post('/register', async function (ctx, next) {
  const { username, password } = ctx.request.body
  if (!username || !password) {
    ctx.body = new ErrorModel('账号或者密码不能为空')
    return
  }
})

router.get('/', function (ctx, next) {
  ctx.body = 'this is a users response!'
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

module.exports = router

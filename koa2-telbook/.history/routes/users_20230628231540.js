const router = require('koa-router')()

router.prefix('/api/users')

router.post('/register', async function (ctx, next) {
  const { username, password }
})

router.get('/', function (ctx, next) {
  ctx.body = 'this is a users response!'
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

module.exports = router

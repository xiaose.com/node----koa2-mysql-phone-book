/**
 * des: 登录中间件
 */

const { ErrorModel, loginFailure } = require("../model/resModel")
const verify = require('../utils/verify')
const { AUTORIZATION } = require('../conf/db')

const { userInfo } = require('../controller/users')

module.exports = async (ctx, next) => {
    // 获取jwt
    const token = ctx.header.authorization
    console.log(ctx.request.url)
    const url = ctx.request.url

    console.log(token, token !== null && token, 'aaa')
    // if () {
    //     // next()
    //     return
    // }

    if (url !== '/api/users/login' && token !== null && token) {
        console.log((token), 'vvv')
        try {
            console.log('aaxxxx')

            // 解密payload 获取用户名和ID
            let payload = await verify(token, AUTORIZATION.jwtSecret)
            console.log(payload)

            if (payload) {
                console.log(payload)
                // 根据用户id 获取用户信息
                let user = await userInfo(payload.id)

                if (!!user) {
                    const userData = {
                        name: payload.username,
                        id: payload.id
                    }
                    ctx.state.user = userData // 存用户数据
                    console.log(ctx, 'ddssds')
                    await next()
                }
            }

        } catch (err) {
            ctx.body = new loginFailure('认证失效，请重新登录')
            // next(ctx)
            return
        }
        console.log('00000')
    } else {
        ctx.body = new ErrorModel('token不能为空')
        return
    }
    console.log('过来')
    // await next()
}
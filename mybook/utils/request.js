import { fail } from "assert"

const baseUrl = 'http://192.168.31.99:3000/api/'

export const getRequest = (api, params)=>{
	return new Promise((resoive,reject) => {
		uni.request({
			url: `${baseUrl}${api}`,
			method:"get",
			header:{
				authorization: sessionStorage.getItem('authorization')
			},
			data: params,
			success: (res) => {
				console.log(res)
				if(res.data.code == 401){
	
					uni.showToast({
						icon:'error',
						title: '登录过期请重新登录'
					})
					uni.redirectTo({
						url: '/pages/login/login'
					})
					return
				} 
				resoive(res.data)
			},
			fail:(er)=>{
				reject(er)
			}
		})
	})
}
export const postRequest = (api, data)=>{
	return new Promise((resoive,reject) => {
		uni.request({
			url: `${baseUrl}${api}`,
			method:"post",
			header:{
				authorization: sessionStorage.getItem('authorization')
			},
			data: data,
			success: (res) => {
				resoive(res.data)
			},
			fail:(er)=>{
				reject(er)
			}
		})
	})
}